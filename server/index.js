var express = require('express');
var jsonServer = require('json-server');
const path = require('path')

var app = express();
var cors = require('cors');
app.use(cors())


//make cdn available to client
app.use(express.static(path.join(__dirname, './public')));

// ...

// You may want to mount JSON Server on a specific end-point, for example /api
// Optiona,l except if you want to have JSON Server defaults
// server.use('/api', jsonServer.defaults()); 
app.use('/api', jsonServer.defaults(), jsonServer.router('db.json'));
// ...


app.listen(8000 , ()=>{
    console.log('JSON Server is running');
});