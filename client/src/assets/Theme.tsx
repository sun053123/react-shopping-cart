import { green, purple } from '@mui/material/colors';
import { createTheme } from '@mui/material/styles';

const theme = createTheme({
	palette: {
		primary: {
			main: green[500],
		},
		secondary:{
			main: purple[500],
		} ,
	},
	typography: {
		fontFamily: 'Roboto',
		fontWeightLight: 300,
		fontWeightRegular: 400,
		fontWeightMedium: 500,
		fontWeightBold: 600,
	},
});

export default theme;