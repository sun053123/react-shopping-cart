import React from 'react'

import {useFetchProducts}  from '../hooks/useProducts'
import ProductCard from '../components/store/card'
import useStateProductsContext from '../context/ProductsContext'
import { Typography, Box, Button, Grid } from '@mui/material'
import { ProductI } from '../types/index'


export function Store() {
  const { productscontext, setProductscontext } = useStateProductsContext()

  const [products, setProducts] = React.useState<ProductI[]>([])

  const onSuccessProducts = (data: ProductI[]) => {
    //set to products state
    //since we have useEffect, this will be called everytime the data changes ,we dont need to set it to products state
    // setProducts(data)
    //set to products context local storage
    setProductscontext(data)
  }
  
  const onErrorProducts = (error: Error) => {
    //TODO: dispatch error
    console.log(error)
  }

  const { isLoading, error, data } = useFetchProducts(onSuccessProducts, onErrorProducts);

  React.useEffect(() => {
    //no fetch , use cache
    if (data) {
      console.log('use cache')
      setProducts(data)
    }
  }
  , [data])

  if (isLoading ) {
    console.log('loading')
    return <Typography>Loading...</Typography>
  }
  if (error) {
    return <Typography>Error: {error.message}</Typography>
  }


  return (
    <Box>
      <Typography component="h2" variant='h2'>Store</Typography>
      <Box sx={{display: 'flex'}}>
        <Grid container spacing={2} >
          {products.map((product: ProductI) => (
            <Grid item xs={12} sm={6} md={4} key={product.id} sx={{mb:2}} >
              <ProductCard product={product}  />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  )
}




