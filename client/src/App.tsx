import React from 'react'
import axios from 'axios'
import { Routes, Route, Navigate, useLocation } from "react-router-dom";
import { CssBaseline, Container } from '@mui/material'

import Nav from './components/Nav'
import {Home} from './pages/Home'
import {Store} from './pages/Store'
import {About} from './pages/About'
import { margin } from '@mui/system';

function App() {

  //axios set http request proxy
  axios.defaults.baseURL = 'http://localhost:8000/api'

  //Tutorial: https://www.youtube.com/watch?v=lATafp15HWA&t=2311s

  return (
    <>
    <CssBaseline />
      <Nav />
      {/* router */}
      <Container sx={{mb: 4}}>
        <Routes>
        <Route  path="/" element={<Home/>} />
        <Route  path="/store" element={<Store/>} />
          <Route  path="/about" element={<About/>} />
        </Routes>
      </Container>
    </>
  )
}

export default App
