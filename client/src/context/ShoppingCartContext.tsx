import { createContext, useContext, ReactNode, useState} from "react"
import {ShoppingCart} from "../components/ShoppingCart"
import { useLocalStorage } from "../hooks/useLocalStorage"

type ShoppingCartProviderProps = {
    children: ReactNode
}

type ShoppingCartContext = {
    openCart: () => void
    closeCart: () => void
    getItemQuantity: (id: number) => number
    increaseCartQuantity: (id: number) => void
    decreaseCartQuantity: (id: number) => void
    removeItem: (id: number) => void 
    cartQuantity: number
    cartItems: CartItem[]
}

type CartItem = {
    id: number
    quantity: number
}

const ShoppingCartContext = createContext({} as ShoppingCartContext)

export function useShoppingCart()  {
    return useContext(ShoppingCartContext)
}

export function ShoppingCartProvider({children}: ShoppingCartProviderProps) {

    const [isOpen, setIsOpen] = useState(false)
    const [cartItems, setCartItems] = useLocalStorage<CartItem[]>("shopping-cart",[])

    const cartQuantity = cartItems.reduce((quantity, item) => {
        return item.quantity + quantity
    }, 0)

    const openCart = () => {
        setIsOpen(true)
    }

    const closeCart = () => {
        setIsOpen(false)
    }


    function getItemQuantity(id: number) {
        //find the item in the cart and return the quantity if it exists or 0 if it doesn't exist (default) 
        const item = cartItems.find(item => item.id === id)
        return item ? item.quantity : 0
    }

    function increaseCartQuantity(id: number) {
        //find the item in the cart and increase the quantity by 1 if it exists or add it to the cart if it doesn't exist
        const item = cartItems.find(item => item.id === id)
        if(item) {
            //if item exists, increase the quantity by 1 and update the cart items array with the new quantity 
            setCartItems(cartItems.map(item => item.id === id ? {...item, quantity: item.quantity + 1} : item))
        }
        else {
            //if item doesn't exist, add it to the cart with a quantity of 1 and update the cart items array with the new item 
            setCartItems([...cartItems, {id, quantity: 1}])
        }
    }

    function decreaseCartQuantity(id: number) {
        //find the item in the cart and decrease the quantity by 1 if it exists or remove it from the cart if it doesn't exist
        const item = cartItems.find(item => item.id === id)
        if(item) {
            //if item exists, decrease the quantity by 1 and update the cart items array with the new quantity
            if(item.quantity > 1) {
                setCartItems(cartItems.map(item => item.id === id ? {...item, quantity: item.quantity - 1} : item))
            }
            else {
                //if quantity is 1, remove the item from the cart and update the cart items array with the new cart items array
                setCartItems(cartItems.filter(item => item.id !== id))
            }
        }
        //if item doesn't exist, do nothing
    }

    function removeItem(id: number) {
        //remove the item from the cart and update the cart items array with the new cart items array
        setCartItems(cartItems.filter(item => item.id !== id))
    }



    return (
        <ShoppingCartContext.Provider value={{
            getItemQuantity,
            increaseCartQuantity,
            decreaseCartQuantity,
            removeItem,
            cartQuantity,
            openCart,
            closeCart,
            cartItems
        }}>
            {children}
            <ShoppingCart isOpen={isOpen} />
        </ShoppingCartContext.Provider>
    )
}

