import { createContext, useContext, useState, useEffect, ReactNode } from "react";

import{ ProductI} from "../types/index";

type ProductsContext = {
    productscontext: ProductI[]
    setProductscontext: (products: ProductI[]) => void
}

export const stateContext = createContext({
    productscontext: [],
    setProductscontext: () => {}
} as ProductsContext);

const getFreshProducts = () => {
    if(localStorage.getItem('products') as string === null) {
        //if no products in local storage, create products local storage
        localStorage.setItem('products', JSON.stringify([]))
    }
    return JSON.parse(localStorage.getItem('products') as string)
}

export default function useStateProductsContext() {
    const { productscontext, setProductscontext } = useContext<ProductsContext>(stateContext);
    return { 
        productscontext,
        setProductscontext: (products: ProductI[]) => {
            console.log('setProductscontext')
            setProductscontext({
                ...productscontext,
                ...products
            })
        },
        resetProductscontext: () => {
            localStorage.setItem('products', JSON.stringify([]))
            setProductscontext(getFreshProducts())
        }
    }
}

export function ProductContextProvider({children}: {children: ReactNode}) {
    const [productscontext, setProductscontext] = useState<ProductI[]>(getFreshProducts());
    

    useEffect(() => {
        localStorage.setItem('products', JSON.stringify(productscontext))
    }, [productscontext])

    return (
        <stateContext.Provider value={{productscontext, setProductscontext}}>
            {children}
        </stateContext.Provider>
    )
}