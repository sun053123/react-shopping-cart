export interface ProductI {
    id: number
    product_name: string
    description?:string
    price: number
    imageurl: string
}