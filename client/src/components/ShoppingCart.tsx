// import * as React from 'react';
// import Box from '@mui/material/Box';
// import Drawer from '@mui/material/Drawer';
// import Button from '@mui/material/Button';
// import List from '@mui/material/List';
// import Divider from '@mui/material/Divider';
// import ListItem from '@mui/material/ListItem';
// import ListItemIcon from '@mui/material/ListItemIcon';
// import ListItemText from '@mui/material/ListItemText';
// import InboxIcon from '@mui/icons-material/MoveToInbox';
// import MailIcon from '@mui/icons-material/Mail';
import { useShoppingCart } from '../context/ShoppingCartContext';

import {
  Drawer,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  List,
} from "@mui/material";
import {
  CheckBoxOutlineBlankOutlined,
  DraftsOutlined,
  HomeOutlined,
  InboxOutlined,
  MailOutline,
  ReceiptOutlined,
} from "@mui/icons-material";
import CartItems from './CartItems';
import { formatCurrency } from '../utils/formatCurrency';
import useStateProductsContext from '../context/ProductsContext';



type ShoppingCartProps = {
  isOpen: boolean;
}

export function ShoppingCart({isOpen}: ShoppingCartProps) {
  const { closeCart, cartItems } = useShoppingCart();

    //get products from the context
    const { productscontext } = useStateProductsContext()
    //object to array
    const products = Object.values(productscontext)

  const handleClose = () => {
    closeCart();
  }

  const getList = () => (
    <div style={{ width: 250 }} onClick={() => handleClose()}>
      {cartItems.map((item) => (
        <CartItems key={item.id} {...item} />
      ))}
    </div>
  );
  return (
    <div>
      <Drawer open={isOpen} anchor={"right"} onClose={() => handleClose()}>
        {getList()}
        {/* Total price */}
        <Divider />
        <ListItem>
          <ListItemText primary="Total" />
          <ListItemText primary={`${formatCurrency(
            cartItems.reduce((total, cartItem) => {
              const item = products.find((p) => p.id === cartItem.id);
              return total + (item?.price || 0) * cartItem.quantity;
            }, 0)
          )}`} />
        </ListItem>
        <Divider />
      </Drawer>
    </div>
  );
}

