import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

//get product interface
import { ProductI } from '../../types/index';
import { formatCurrency } from '../../utils/formatCurrency';
import { useShoppingCart } from '../../context/ShoppingCartContext';

export default function ProductCard({ product }: { product: ProductI }) {
    const { getItemQuantity, increaseCartQuantity, decreaseCartQuantity, removeItem } = useShoppingCart();

    const quantity = getItemQuantity(product.id);

    return (
        <Card sx={{ maxWidth: 345 }}>
            
            <CardMedia
                component="img"
                height="200"
                image={product.imageurl}
                alt="product image"
            />
                        {/* delete from cart button on top of image */}
 
                  
            <CardContent sx={{
                display: 'flex',
                justifyContent: 'space-between',
            }}>
                <Typography gutterBottom variant="h5" component="div">
                    {product.product_name}
                </Typography>
                <Typography gutterBottom variant="h6" component="div">
                    {formatCurrency(product.price)}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Share</Button>
                <Button size="small">Learn More</Button>
            </CardActions>
            {quantity === 0 ?
                <Button fullWidth variant='contained' onClick={() => increaseCartQuantity(product.id)}>Add to cart</Button> :
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                    <Button variant='contained' color='primary' onClick={() => {
                        increaseCartQuantity(product.id);
                    }}>Add to cart</Button>
                    <Typography variant="h6" color="inherit">

                        {quantity}
                    </Typography>
                    <Button variant='contained' color='secondary' onClick={() => {
                        decreaseCartQuantity(product.id);
                    }
                    }>Remove from cart</Button>                    
                    <Box sx={{
                        position: 'block',
                        top: '0',
                        right: '0',
                        backgroundColor: '#d32f2f',
                        color: 'white',
                        padding: '5px',
                        fontSize: '10px',
                        fontWeight: 'bold',
                        borderRadius: '50%',
                        cursor: 'pointer',
                        '&:hover': {
                            backgroundColor: '#b71c1c',
                        }
                    }}
                    onClick={()=>{
                        removeItem(product.id);
                    }}>
                        <Typography variant="button" color="inherit">
                            X
                        </Typography>
                    </Box>
                </Box>


            }
        </Card>
    );
}
