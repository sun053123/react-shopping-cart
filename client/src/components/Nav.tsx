import React from 'react'

import { AppBar, Typography, Box, Toolbar, Button, IconButton } from '@mui/material'
import { Link } from 'react-router-dom';
import MenuIcon from '@mui/icons-material/Menu';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useShoppingCart } from '../context/ShoppingCartContext';

import { ShoppingCart } from './ShoppingCart';

function Nav() {

    const [counter2, setCounter2] = React.useState<number>(0)
    const {openCart, cartQuantity} = useShoppingCart()

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" sx={{
                backgroundColor: 'white',
                color: '#000',
            }}>
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2, }}
                    >
                        <MenuIcon />
                    </IconButton>

                    <Box sx={{ margin: 'auto' }}>
                        <Button color="inherit" component={Link} to="/">Home</Button>
                        <Button color="inherit" component={Link} to="/about">About</Button>
                        <Button color="inherit" component={Link} to="/store">Store</Button>
                    </Box>

                    <Button onClick={() => {
                        setCounter2(counter2 + 1)
                    }}>
                        {counter2}
                    </Button>

                    <Button color="primary" variant="outlined" onClick={
                        () => {
                            openCart()
                        }
                    } >
                        <ShoppingCartIcon />
                        <Typography variant="button" color="inherit">
                            Cart
                        </Typography>
                        {/* rounded cart item value*/}
                        <Typography variant="button" color="inherit">
                            $0.00
                        </Typography>
                        <Typography variant="button" sx={{
                            backgroundColor: '#d32f2f',
                            //circle
                            borderRadius: '50%',
                            marginLeft: '10px',
                            padding: '5px',
                            color: 'white',
                            fontSize: '10px',
                            fontWeight: 'bold',
                            position: 'absolute',
                            top: '50%',
                            right: '-10px',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',

                        }}>
                            ({cartQuantity})
                        </Typography>

                    </Button>

                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default Nav