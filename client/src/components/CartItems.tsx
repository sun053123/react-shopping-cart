import { Button, ListItem, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import useStateProductsContext from '../context/ProductsContext'
import { useShoppingCart } from '../context/ShoppingCartContext'
import { ProductI } from '../types'

type CartItemsProps = {
    id: number
    quantity: number
}

function CartItems({id, quantity}: CartItemsProps) {
    const { removeItem } = useShoppingCart()

    //get products from the context
    const { productscontext } = useStateProductsContext()
    //object to array
    const products = Object.values(productscontext)

    //find products context by id
    const product = products.find((p: { id: number }) => p.id === id) || {} as ProductI
    if (!product) {
        console.log('product not found')
        return null
    }
    

  return (
    <ListItem>
      <Box display="flex" flexDirection="row" alignItems="center">
        <Box mr={2}>
            <img src={product.imageurl} alt={product.product_name} width={50} height={50} />
        </Box>
        <Box>
            <Typography variant="h6">{product.product_name}</Typography>
            <Typography variant="h6">{product.price * quantity}</Typography>
        </Box>
        <Box ml={2}>
            <Typography variant="h6">{quantity}</Typography>
        </Box>
        <Button variant="outlined" color="secondary" size='small' onClick={() => removeItem(id)}>X</Button>
        </Box>
    </ListItem>
  )
}

export default CartItems