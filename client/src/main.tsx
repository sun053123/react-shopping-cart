import { ThemeProvider } from '@mui/material/styles'
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'

import { BrowserRouter } from 'react-router-dom'
import Theme from './assets/Theme'
import { ProductContextProvider } from './context/ProductsContext'
import { ShoppingCartProvider } from './context/ShoppingCartContext'
const queryClient = new QueryClient()

ReactDOM.createRoot(document.getElementById('root')!).render(
  <QueryClientProvider client={queryClient}>
    <ReactQueryDevtools initialIsOpen={true} />
    <ThemeProvider theme={Theme}>
      <ProductContextProvider>
        <ShoppingCartProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
      </ShoppingCartProvider>

      </ProductContextProvider>
    </ThemeProvider>
  </QueryClientProvider>
)
