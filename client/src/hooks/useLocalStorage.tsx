import { useState, useEffect } from 'react'

export function useLocalStorage<T>(key: string, initialValue: T | (() => T)) {
    const [storedValue, setStoredValue] = useState<T>(() => {
        const jsonValue = localStorage.getItem(key)
        if (jsonValue) {
            return JSON.parse(jsonValue)
        }
        if (typeof initialValue === 'function') {
            return (initialValue as () => T)()
        } else {
            return initialValue
        }
    });

    useEffect(() => {
        // Update the localStorage value every time the state changes (on mount, or after a state change) to keep the value in sync.
        localStorage.setItem(key, JSON.stringify(storedValue))
    },[storedValue, key]);

    return [storedValue, setStoredValue] as [T, typeof setStoredValue]

}