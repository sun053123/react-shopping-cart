import { useQuery } from 'react-query'
import axios from 'axios'
import { formatAxiosData } from '../utils/formatAxiosData'
import { ProductI } from '../types/index'
import { PRODUCTS_API } from '../api/index'


const getProducts = async () => {

    // delay for demo 0.5sec
    await new Promise(resolve => setTimeout(resolve, 500))

    const response = await axios.get(`${PRODUCTS_API}`)
    return response.data
}

export const useFetchProducts = ( onSuccess : (data: ProductI[]) => void, onError : (error: Error) => void ) => {
    return useQuery<ProductI[], Error>('products', getProducts, {
        refetchOnWindowFocus: false,
        //keep data in cache for 10 sec 
        staleTime: 10000,
        onSuccess,
        onError,
        select: (data: ProductI[]) => {
            return data
        }
    })
}


